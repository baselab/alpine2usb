#!/bin/bash

#set -x
set -e

VERSION="1.0"
PROG="${0}"
PDESC="Put Alpine Linux to USB pendrive"

USER="$(id -u)"
ISODIR="/tmp/isodir${RANDOM}"
DEVDIR="/tmp/devdir${RANDOM}"
yesman=("y" "Y" "yes" "Yes" "YES")

help() {
    cat <<EOF

${PROG} ${VERSION}
${PDESC}

    Syntax: sudo ${BASH} ${PROG} </path/to/iso> </path/to/dev>

EOF
}

# User id check
if [[ "${USER}" != 0 ]] ; then
    echo "Please run me as root."
    exit 1
fi

# Args check
if [[ "${#}" != 2 ]] ; then
    help
    exit 1
fi

# Abort function
cont() {
    if [[ -z "${1}" ]] ; then
        echo "Abort."
        exit 1
    elif [[ "${yesman[@]}" =~ "${1}" ]] ; then
        echo -n #otherwise?
    else
        echo "Abort."
        exit 1
    fi
}

ISO="${1}"
DEV="${2}"

# ISO and device files check
if [[ ! -f "${ISO}" ]] ; then
    echo "Cannot read ${ISO}"
    cont not
elif [[ ! -b "${DEV}" ]] ; then
    echo "${DEV} not a block device"
    cont not
fi

echo "Checking ISO content... "
mkdir ${ISODIR}
mount -o loop ${ISO} ${ISODIR}

if [[ ! -f "${ISODIR}/.alpine-release" ]] ; then
    echo "${ISO} is not Alpine Linux!"
    umount ${ISODIR}
    rmdir ${ISODIR}
    cont not
else
    echo "OK"
fi

# choosing first mbr found, it may create chaos
echo "Searching syslinux MBR..."
MBR="$(find /usr/ -type f -name "mbr.bin" | head -1)"

if [[ ! -f "${MBR}" ]] ; then
    echo "Cannot find mbr.bin in /usr/..."
    cont not
else
    echo "Found: ${MBR}"
fi

echo "ISO image is ${ISO}"
echo "Device is ${DEV}"
read -p "Continue? (y/N) " YN
cont ${YN}

echo "Partitioning device... "
parted -a min -- ${DEV} mktable msdos
parted -a min -- ${DEV} mkpart primary 1 -0
echo "OK"

# any better way?
PART="$(lsblk -ln -o NAME ${DEV} | tail -1)"

echo "Formatting partition... "
mkfs.vfat /dev/${PART}
echo "OK"

echo "Installing syslinux on device... "
dd if=${MBR} of=${DEV}
syslinux /dev/${PART}
echo "OK"

echo "Copying content... "
mkdir ${DEVDIR}
mount -t vfat /dev/${PART} ${DEVDIR}

cp -a ${ISODIR}/.alpine-release ${DEVDIR}/
cp -a ${ISODIR}/* ${DEVDIR}/
echo "OK"

echo "Cleaning..."
umount ${ISODIR}
rmdir ${ISODIR}
umount ${DEVDIR}
rmdir ${DEVDIR}

sync

echo "Done"
