
#alpine2usb
*Put Alpine Linux on USB pendrive*

Since Alpine Linux maintainers do not create hybrid ISO images that you
can just dd to pendrives, I've put together some commands to easy the pain.

Enjoy.

Best results on Debian Gnu/Linux or derivatives.

### Dependencies

* Alpine Linux ISO
* A pendrive (512MB is enough!)
* Bash >= 4
* parted
* syslinux
* dosfstools

### Usage

`sudo bash alpine2usb.sh </path/to/iso> </path/to/dev>`

**This will completely erase the pendrive.**
